$(document).ready( function() {
    $('#txtnote').keypress(function(e) {
        if(e.which == 13) {
            $(this).blur();
            $('#submit').focus().click();
        }
    });

    $( '#submit' ).click(function(e) {
        var notes_taken = $('#notes-taken'),
            new_note = document.createElement( 'div' ),
            txt_note = $('#txtnote'),
            close = document.createElement('span');

        if ($(txt_note).val().length > 0) {
            $(new_note).html(txt_note.val());
            $(new_note).addClass('note yellow');
            $(new_note).append(close);
            $(notes_taken).append(new_note);
            $(txt_note).val('');
            $(txt_note).focus();
        }
        e.preventDefault();
    });


    $('#notes-taken').on('click', '.note', function() {
       $(this).toggleClass('crossed');
    });

    $('#notes-taken').on('click', 'span', function() {
        $(this).parent().hide(2000);
     });
});
